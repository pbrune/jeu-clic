import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'truthapp';

  // garçon ou fille
  garconOuFille?:string;

  // amoureux, ami, neutre, ou ennemi
  relation?: string;

  numeroPersonne?: number

  noms:{[index:string]:string[]} = {
    'fille':[
      'Solène',
      'Chloé',
      'Isabelle',
      'Laurianne',
      'Amelia',
      'Sophie',
      'Lyana',
      'Valérie'
    ],
    'garçon':[
      'Samuel',
      'Toto',
      'Alexandre',
      'Emersohn',
      'Wolf',
      'Isaiah',
      'Paul',
      'Achille',
      'Charles-Édouard',
    ]
  }

  relationFille:{[index:string]:string} = {
    'ami': 'amie',
    'amoureux': 'amoureuse',
    'neutre': 'neutre',
    'ennemie': 'ennemie',
  }

  phrase?:string;

  changePersonne(ev:any) {
    this.numeroPersonne = ev.target.value;
  }

  afficheResultat() {

    if(!this.garconOuFille){
      throw Error('Garçon ou fille pas choisi.');
    }
    if(!this.numeroPersonne){
      throw Error('Numréo pas choisi.');
    }
    if(!this.relation){
      throw Error('Relation pas choisie.');
    }

    const nom = this.noms[this.garconOuFille][this.numeroPersonne];

    this.phrase = nom + ' est ton ' + (this.garconOuFille == 'garçon' ? this.relation : this.relationFille[this.relation]);

  }

  reset() {
    this.garconOuFille = undefined;
    this.relation = undefined;
    this.numeroPersonne = undefined;
    this.phrase = undefined;
  }
}
